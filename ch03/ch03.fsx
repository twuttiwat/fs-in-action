let addTenThenDouble theNumber =
    let addedTen = theNumber + 10
    let answer = addedTen * 2
    printfn $"({theNumber} + 10) * 2 is {answer}"
    let website = System.Uri "https://fsharp.org"
    answer

addTenThenDouble 42    

let fname = "Frank"
let sname = "Schmidt"
let fullName = $"{fname} {sname}"
let greetingText = $"Greetings, {fullName}"

let greetingText' =
    let fullName = 
        let fname = "Frank"
        let sname = "Schmidt"
        $"{fname} {sname}"
    $"Greetings, {fullName}"

greetingText = greetingText'    

let greetingTextWithFunction person =
    let makeFullName fname sname = 
        $"{fname} {sname}"
    let fullName = makeFullName "Frank" "Schmidt"       
    $"Greetings {fullName} from {person}."

greetingTextWithFunction "Roger"    

// let error : string = 10
let i = 10
let j = "Hello"
let k = 123.456
let l = System.DateTime.Now

let add (a:int) (b:int) : int =
    let answer : int = a + b
    answer

let explicit = ResizeArray<int>()
let typeHole = ResizeArray<_>()
let omitted = ResizeArray()
omitted.Add "10"    

let combineElements<'T> (a:'T) (b:'T) (c:'T) =
    let output = ResizeArray<'T>()
    output.Add a
    output.Add b
    output.Add c
    output

combineElements<int> 1 2 3

combineElements<int> 1 2 "test"
// Error: This expression was expected to have type 'int' but here has type 'string'

let combineElements' a b c =
    let output = ResizeArray<'T>()
    output.Add a
    output.Add b
    output.Add c
    output

combineElements' 1 2 3

let calculateGroup age =
    if age < 18 then "Child"
    elif age < 65 then "Adult"
    else "Pensioner"

let sayHello someValue = 
    let group =
        if someValue < 10.0 then calculateGroup 15
        else calculateGroup 35
    "Hello " + group

let result = sayHello 10.5

let addThreeDays (theDate:System.DateTime) =
    theDate.AddDays 3

let addAYearAndThreeDays theDate =
    let threeDaysForward = addThreeDays theDate
    theDate.AddYears 1