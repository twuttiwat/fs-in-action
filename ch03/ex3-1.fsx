let funA a b c =
    let inProgress = a + b
    let answer = inProgress * c
    $"The answer is {answer}"

funA 1 2 3

let funB () =
    ["a"; "b"]
    |> List.map id
    |> List.filter (fun x -> x = "a")