let executeFunction a b =
    let answer = a + b
    printfn $"Adding {a} and {b} = {answer}"
    answer

executeFunction 1 2    

let executeFunctionWithLogger logger a b =
    let answer = a + b
    logger $"Adding {a} and {b} = {answer}"
    answer

let consoleLogger msg = printfn "%s" msg

executeFunctionWithLogger consoleLogger 1 2    

let fileLogger msg =
    let path = __SOURCE_DIRECTORY__ + @"\log.txt"
    System.IO.File.AppendAllText(path, msg)

executeFunctionWithLogger fileLogger 1 2    

executeFunctionWithLogger (fun msg -> printfn "%s" msg) 1 2    

type Result =
    { HomeTeam : string; HomeGoals : int
      AwayTeam : string; AwayGoals : int }
let create home hg away ag =
    { HomeTeam = home; HomeGoals = hg
      AwayTeam = away; AwayGoals = ag }
let results = [
    create "Messiville" 1 "Ronaldo City" 2
    create "Messiville" 1 "Bale Town" 3
    create "Ronaldo City" 2 "Bale Town" 3
    create "Bale Town" 2 "Messiville" 1
]

type TeamSummary = { Name : string; mutable AwayWins : int }
let summary = ResizeArray<TeamSummary>()

summary.Clear()
for result in results do
    if result.AwayGoals > result.HomeGoals then
        let mutable found = false
        for entry in summary do
            if entry.Name = result.AwayTeam then
                found <- true
                entry.AwayWins <- entry.AwayWins + 1
        if not found then
            summary.Add { Name = result.AwayTeam; AwayWins = 1 }

let mutable wonAwayTheMost = summary[0]
for row in summary do
    if row.AwayWins > wonAwayTheMost.AwayWins then wonAwayTheMost <- row            

wonAwayTheMost        

let isAwayWin result = result.AwayGoals > result.HomeGoals

let wonAwayTheMost1 =
    results
    |> List.filter isAwayWin
    |> List.countBy (fun result -> result.AwayTeam)
    |> List.sortByDescending (fun (_, wins) -> wins)
    |> List.head

let wonAwayTheMost2 =
    results
    |> List.filter isAwayWin
    |> List.countBy (fun result -> result.AwayTeam)
    |> List.maxBy (fun (_,wins) -> wins)

let ronaldoNumGames =    
    results   
    |> List.filter (fun r -> r.AwayTeam = "Ronaldo City" || r.HomeTeam = "Ronaldo City")
    |> List.length

let nums = [1..10]
nums |> List.map string
nums |> List.collect (fun n -> [1..n])
nums |> List.filter (fun n -> n % 2 = 0)
nums |> List.skip 5
nums |> List.take 5
nums |> List.sort
nums |> List.groupBy (fun n -> n % 2)
nums |> List.windowed 3
nums |> List.partition (fun n -> n % 2 = 0)

let numbers = [ 1 .. 10 ]
let secondItem = numbers[1]
let itemsOneToSix = numbers[1..5]

let numbersArr = [| 1 .. 10 |]
let secondElement = numbersArr[1]
let squares = numbersArr |> Array.map (fun x -> x * x)

let numbersSeq = seq { 1 .. 10 }
let secondElementSeq = numbersSeq |> Seq.item 1
let squaresSeq = numbersSeq |> Seq.map (fun x -> x * x)

let data = [ "Foo", "Bar"; "Baz", "Quux"]
let lookup = readOnlyDict data
let fooVal = lookup["Foo"]
lookup["Foo"] <- "QWERTY"

let lookupMap = Map data
let newLookupMap = lookupMap.Add("Fuz", "Fjord")
let newLookupMap = lookupMap |> Map.add "Foo" "QWERTY"

type Employee = { Name : string }
let salesEmployees = Set [ { Name = "Foo" }; { Name = "Bar" }]
let bonusEmployees = Set [ { Name = "Foo" }; { Name = "Quux" }]

let allBonusesForSalesStaff =
    salesEmployees |> Set.isSubset bonusEmployees

let salesWithoutBonuses =
    salesEmployees - bonusEmployees        

#time
let largeComputation =
    seq { 1 .. 100_000_000}
    |> Seq.map (fun x -> x * x)
    |> Seq.rev
    |> Seq.filter (fun x -> x % 2 = 0)
    |> Seq.toArray

let comp = seq {
    1
    2
    if System.DateTime.Today.DayOfWeek = System.DayOfWeek.Tuesday then 99
    4    
}    

let numbers = [ 1 .. 10 ] |> List.map string

for n in numbers do
    printfn $"Number {n}"

for n in 1 .. 10 do
    printfn "Number %i" n

let mutable counter = 1
while counter <= 10 do
    printfn $"Number {counter}"
    counter <- counter + 1

let inputs = [ 1 .. 10 ]    
let sum inputs =
    let mutable accumulator = 0
    for input in inputs do
        accumulator <- accumulator + input
    accumulator

let sum1 inputs =
    inputs
    |> Seq.fold
        (fun accumulator input -> 
            printfn $"Acc {accumulator} + input {input} =>"
            accumulator + input)    
        0
sum1 inputs        

open System
let allDates = seq {
    let mutable theDate = DateTime.MinValue
    while theDate <= DateTime.MaxValue do
        theDate
        theDate <- theDate.AddDays 1
}   

let mondays =
    allDates
    |> Seq.skipWhile (fun d -> d.Year < 2020 || d.Month < 2)
    |> Seq.filter (fun d -> d.DayOfWeek = DayOfWeek.Monday)
    |> Seq.takeWhile (fun d -> d.Year = 2020 && d.Month < 6)    
    |> Seq.toArray

let userInput = seq {
    printfn "Enter command (X to exit)"
    while true do
        System.Console.ReadKey().KeyChar
}    

let processInputCommands commands =
    commands
    |> Seq.takeWhile (fun cmd -> cmd <> 'x')
    |> Seq.iter (fun cmd ->
        printfn ""
        if cmd = 'w' then printfn "Withdrawing money!"
        elif cmd = 'd' then printfn "Depositing money!"
        else printfn $"You executed command {cmd}"    
    )