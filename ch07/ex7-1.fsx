let calculate op inputOne inputTwo opName =
    let answer = op inputOne inputTwo
    printfn $"{opName} {inputOne} {inputTwo} = {answer}"
    answer

calculate (fun a b -> a + b) 1 2 "Add"
calculate (fun a b -> a - b) 1 2 "Subtract"

