type Result =
    { HomeTeam : string; HomeGoals : int
      AwayTeam : string; AwayGoals : int }
let create home hg away ag =
    { HomeTeam = home; HomeGoals = hg
      AwayTeam = away; AwayGoals = ag }
let results = [
    create "Messiville" 1 "Ronaldo City" 2
    create "Messiville" 1 "Bale Town" 3
    create "Ronaldo City" 2 "Bale Town" 3
    create "Bale Town" 2 "Messiville" 1
]

let scoreTheMost =
    results    
    |> List.collect (fun r -> [ (r.HomeTeam, r.HomeGoals); (r.AwayTeam, r.AwayGoals) ])
    |> List.groupBy fst
    |> List.map (fun (team, teamGoals) -> (team, teamGoals |> List.sumBy snd) )
    |> List.maxBy snd
    |> fst