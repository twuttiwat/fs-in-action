let drive gas distance =
    if distance > 50.0 then gas / 2.0
    elif distance > 25.0 then gas - 10.0
    elif distance > 1.0 then gas - 10.0        
    else gas

let gas = 100.0
let firstState = drive gas 100
let secondState = drive firstState 40
let thirdState = drive secondState 10
let finalState = drive thirdState 0



