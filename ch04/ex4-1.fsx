type Employee() =
    let mutable salary = 0.0
    member _.SetSalary(newSalary) = salary <- newSalary
    member _.GetSalary() = salary

let e1 = Employee()
e1.SetSalary(42.0) // Statement
e1.GetSalary()

// Expression
let add a b = a + b

type Customer = { Name:string }

// Statement
let saveToDb customer = ()

// Statment
let saveCustomer1 customer = saveToDb customer 

let isCustomeExists customer = true

// Expression
let saveCustomer2 customer = 
    if isCustomeExists customer then false 
    else 
        do saveToDb  customer
        true
