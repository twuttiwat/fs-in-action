let describeAge age =
    let ageDescription =
        if age < 18 then "Child"
        elif age < 65 then "Adult"
        else "OAP"
    let greeting = "Hello"
    $"{greeting}! You are a '{ageDescription}'"           

describeAge 42    

let getTheCurrentTime = System.DateTime.Now
let x = getTheCurrentTime
let y = getTheCurrentTime

let getTheCurrentTime' () = System.DateTime.Now
let x' = getTheCurrentTime' ()
let y' = getTheCurrentTime' ()

let addDays days=
    let newDays = System.DateTime.Today.AddDays days
    printfn $"{days} become {newDays}"
    newDays

addDays 3    

let addSeveralDays () =
    ignore (addDays 3)
    ignore (addDays 5)
    addDays 7

addSeveralDays ()    

let mutable gas = 100.0
let drive distance =
    if distance = "far" then gas <- gas / 2.0
    elif distance = "medium" then gas <- gas - 10.0
    else gas <- gas - 1.0

drive "far"
drive "medium"
drive "short"
gas        

let drive1 gas distance =
    if distance = "far" then gas / 2.0
    elif distance = "medium" then gas - 10.0
    else gas - 1.0

let gas1 = 100.0
let firstState = drive1 gas1 "far"
let secondState = drive1 firstState "medium"
let finalState = drive1 secondState "short"







