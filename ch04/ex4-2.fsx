let ageDescription age =
    if age < 18 then "Child"
    elif age < 65 then "Adult"
    else "OAP"

let describeAge age =
    let greeting = "Hello"
    $"{greeting}! You are a '{ageDescription age}'"           

describeAge 42    