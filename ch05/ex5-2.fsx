type Person = 
    {
        Name : string * string
        Age : int
        AgeDescription : string
    }

let buildPerson (forename:string) (surname:string) (age:int) =
    let ageDescription = if age < 18 then "child" else "adult"

    {
        Name = forename, surname
        Age = age
        AgeDescription = ageDescription
    }

let p1 = buildPerson "foo" "bar" 10

let {Name = name; Age = age; AgeDescription = ageDesc} = buildPerson "foo" "bar" 30

let {Name = fname,sname; Age = age1; AgeDescription = ageDesc1} = buildPerson "foo" "bar" 30
