
let buildPerson (forename:string) (surname:string) (age:int) =
    let ageDescription = if age < 18 then "child" else "adult"
    (forename, surname), age, ageDescription

let p1 = buildPerson "foo" "bar" 10
let (f1, s1), a1, ad1 = buildPerson "foo" "bar" 30
