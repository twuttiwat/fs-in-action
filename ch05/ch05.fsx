let name = "isaac", "abraham"
let firstName, secondName = name

let nameAndAnge = "Jane", "Smith", 25
let forename, surname, _ = nameAndAnge

let makeDoctor (name:string * string) =
    let _, sname = name
    "Dr", sname

let makeDoctor1 (_, sname) =
    let _, sname = name
    "Dr", sname    

let nameAndAge1 = ("Joe", "Bloggs"), 28
let name1, age1 = nameAndAge1
let (forename1, surname1), theAge1 = nameAndAge1

let makeDoctor2 (name:struct (string * string)) =
    let struct (_, sname) = name
    struct ("Dr", sname)

let struct (name2, surname2) = makeDoctor2 (struct ("foo","boo"))

type Address =
    {
        Line1 : string
        Line2 : string
        Town : string
        Country : string
    }

type Person =
    {
        Name : string * string
        Address : Address
    }    

let theAddress : Address = 
    {
        Line1 = "Line1"
        Line2 = "Line2"
        Town = "Town"
        Country = "Country"
    }

let foo = { Name = "Foo","Bar"; Address = theAddress }    
let foo2 = { Name = "Foo","Bar"; Address = theAddress }    
let foo3 = { Name = "Foo3","Bar"; Address = theAddress }    

foo = foo2
foo = foo3

let company =
    {|
        Name = "My Company Inc."
        Town = "The Town"
        Country = "UK"
        TaxNumber = 123456
    |}

let companyWithBankDetails =
    {|
        company with 
            AccountNumber = 123
            SortCode = 456
    |}   

type Person = 
    {
        Name: string * string
        Address :
            {|
                Line1: string
                Line2: string
            |}
    }