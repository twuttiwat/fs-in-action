type Address =
    {
        Line1 : string
        Line2 : string
        Town : string
        Country : string
    }

type Name = string * string

type Customer =
    {
        Name : Name
        Address : Address
        CreditRating : int
    }    

type Supplier =
    {
        Name : Name
        Address : Address
        OutstandingBalance : decimal
        NextDueDate : System.DateTime
    }

    
