module Car

type DriveResult =
    {
        RemainingGas : int
        OutOfGas : bool
    }


let drive distance gas =    
    let remainingGas = 
        if distance > 50 then gas - 25
        else if distance > 25 then gas - 10
        else if distance > 0 then gas - 1
        else gas

    { 
        RemainingGas = System.Math.Abs remainingGas; 
        OutOfGas = remainingGas <= 0 
    } 
    
            
            

