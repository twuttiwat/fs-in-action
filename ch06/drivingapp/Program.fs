﻿open System
open Car

let initialGas = 10

let drive' distance = drive distance initialGas

printf "Enter Distance: "
let driveResult = 
    Console.ReadLine()
    |> int
    |> drive' 

if driveResult.OutOfGas then
    printfn "You have No gas left."
else        
    printfn $"You have {driveResult.RemainingGas} gas left."