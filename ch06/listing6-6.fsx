namespace Foo
type Order = { Name : string }

namespace Bar.Baz
type Customer = 
    {
        Name : string
        LastOrder : Foo.Order
    }

open Foo
type Customer1 = { LastOrder : Order }

