let add (firstNumber:int) (secondNumber:int) = firstNumber + secondNumber

let addTen = add 10

let multiply (firstNumber:int) (secondNumber:int) = firstNumber * secondNumber

let timesTwo x = multiply x x

let addTenAndDouble x = timesTwo (addTen x)

let firstValue = add 5 10
let secondValue = add firstValue 7
let finalValue = multiply secondValue 2
let finalValueChained = multiply (add (add 5 10) 7) 2

let pipelineSingleLine = 10 |> add 5 |> add 7 |> multiply 2

let pipelineMultiline =
    10
    |> add 5
    |> add 7
    |> multiply 2

    

