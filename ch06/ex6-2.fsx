let drive distance gas =
    if distance > 50 then gas - 50
    else if distance > 25 then gas - 10
    else if distance > 1 then gas - 1
    else gas

100
|> drive 55
|> drive 26
|> drive 1
