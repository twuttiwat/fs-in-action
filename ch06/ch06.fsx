let add (firstNumber:int) (secondNumber:int) = firstNumber + secondNumber
let addFive = add 5
let fifteen = addFive 10
let multiply a b = a * b
let addFiveAndDouble input = input |> add 5 |> multiply 2
let addFiveAndDoubleShort = add 5 >> multiply 2


